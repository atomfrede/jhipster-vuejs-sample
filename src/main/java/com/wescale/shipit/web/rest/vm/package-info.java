/**
 * View Models used by Spring MVC REST controllers.
 */
package com.wescale.shipit.web.rest.vm;
