export default {

    readAuthors(context) {

        let resourceUrl = 'api/authors/';

        let dataPromise = context.$http.get(resourceUrl).then(function(result) {
            console.log('readAuthors then');
            console.log(result);
            let data = result.data;
            // if (data) {
            //     data = JSON.parse(data);
            // }
            return data;
        });
        return dataPromise;
    }
}
