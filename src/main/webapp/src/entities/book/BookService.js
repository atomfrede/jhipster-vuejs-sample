export default {

    readBooks(context) {

        let resourceUrl = 'api/books/';

        let dataPromise = context.$http.get(resourceUrl).then(function(result) {
            console.log('readBooks then');
            console.log(result);
            let data = result.data;
            // if (data) {
            //     data = JSON.parse(data);
            // }
            return data;
        });
        return dataPromise;
    }
}
